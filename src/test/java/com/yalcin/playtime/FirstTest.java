package com.yalcin.playtime;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FirstTest {

	private First first;

	@BeforeEach
	void setup() {
		first = new First();
		assertNotNull(first);
	}

	@Test
	void test() {
		assertEquals(3, first.get3());
		assertNotEquals(45, first.get3());
	}

}
